#include "gazebo_moving_platform.h"

using namespace std;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)

void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);

gazebo::ModelPush modelPush;

/////////////////////////////////////////////////
/// \brief ModelPush::ModelPush
///
ModelPush::ModelPush()
{
}

/////////////////////////////////////////////////
/// \brief RayPlugin::~RayPlugin
///
ModelPush::~ModelPush()
{
}

void ModelPush::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
        // Store the pointer to the model
        this->model = _parent;

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&ModelPush::OnUpdate, this, _1));

        // Exit if no ROS
        if (!ros::isInitialized())
        {
            gzerr << "Not loading Optical Flow plugin since ROS hasn't been "
                  << "properly initialized.  Try starting gazebo with ros plugin:\n"
                  << "  gazebo -s libgazebo_ros_api_plugin.so\n";
            return;
        }

        if (_sdf->HasElement("robotNamespace"))
          namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
        else
          gzwarn << "[gazebo_moving_platform] Please specify a robotNamespace.\n";

        this->node_handle_ = new ros::NodeHandle(namespace_);

        this->moving_sub = this->node_handle_->subscribe("clock", 10, getSimulationClockTime);
    }

    // Called by the world update start event
void ModelPush::OnUpdate(const common::UpdateInfo & /*_info*/)
    {
        // Get Sim time
        common::Time  time = common::Time::GetWallTime();
        double sec = modelPush.time_sec + modelPush.time_nsec / MILLION;
        //cout << sec << endl;

        double A = 4;
        double T = 4 * M_PI;
        double w = 2 * M_PI / T;
        double vel = A*w*cos(w*sec);
        // Apply a small linear velocity to the model.
        this->model->SetLinearVel(math::Vector3(vel, 0, 0));

        // Get world pose
        //       math::Pose pose = this->model->GetWorldPose();
        //       math::Vector3 pos = pose.pos;
        //       cout << "Moving Platform current pose" << endl;
        //       cout << "x: " << pos.x << "y: " << pos.y << "z: " << pos.z << endl;



    }


void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg){

    ros::Time time = msg->clock;

    modelPush.time_sec = time.sec;
    modelPush.time_nsec = time.nsec;

    //cout << "time secs: " << modelPush.time_sec << endl;
}
