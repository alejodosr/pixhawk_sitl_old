/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/*
* Extended Plugin for using both Optical Flow and LIDAR information (px4Flow)
* Author: Alejandro Rodríguez Ramos
* Date: 27th January, 2016
* Email: alejandro.dosr@gmail.com
*
* Description: This extention makes the Optical Flow plugin to publish its information
* via ROS Publisher.
*
* File: gazebo_px4Flow_plugin.cpp
*
*/

#ifdef _WIN32
  // Ensure that Winsock2.h is included before Windows.h, which can get
  // pulled in by anybody (e.g., Boost).
#include <Winsock2.h>
#endif

/*  Includes */

#include "gazebo/sensors/DepthCameraSensor.hh"
#include "gazebo_px4Flow_plugin.h"


#include <cv.h>
//#include <highgui.h>
#include <math.h>
#include <string>
#include <iostream>

using namespace cv;
using namespace std;

using namespace gazebo;


GZ_REGISTER_SENSOR_PLUGIN(CameraPlugin)

/////////////////////////////////////////////////
/// \brief CameraPlugin::CameraPlugin
///
CameraPlugin::CameraPlugin()
: SensorPlugin(), width(0), height(0), depth(0)
{
}

/////////////////////////////////////////////////
/// \brief CameraPlugin::~CameraPlugin
///
CameraPlugin::~CameraPlugin()
{
  this->parentSensor.reset();
  this->camera.reset();
}


/////////////////////////////////////////////////
/// \brief CameraPlugin::Load
/// \param _sensor
/// \param _sdf
///
void CameraPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
    // Exit if no ROS
    if (!ros::isInitialized())
    {
        gzerr << "Not loading Optical Flow plugin since ROS hasn't been "
              << "properly initialized.  Try starting gazebo with ros plugin:\n"
              << "  gazebo -s libgazebo_ros_api_plugin.so\n";
        return;
    }

  if (!_sensor)
    gzerr << "Invalid sensor pointer.\n";

  this->parentSensor =
    boost::dynamic_pointer_cast<sensors::CameraSensor>(_sensor);


  if (!this->parentSensor)
  {
    gzerr << "CameraPlugin requires a CameraSensor.\n";
    if (boost::dynamic_pointer_cast<sensors::DepthCameraSensor>(_sensor))
      gzmsg << "It is a depth camera sensor\n";
  }

  this->camera = this->parentSensor->GetCamera();

  if (!this->parentSensor)
  {
    gzerr << "CameraPlugin not attached to a camera sensor\n";
    return;
  }

  this->width = this->camera->GetImageWidth();
  this->height = this->camera->GetImageHeight();
  this->depth = this->camera->GetImageDepth();
  this->format = this->camera->GetImageFormat();
  this->camera_name =this->camera->GetName();

  if(this->camera_name == "camera_cam"){
      gzwarn << "[gazebo_optical_flow_plugin] This plugin is intended to work as a normal camera \n";
  }
  else if(this->camera_name == "camera_flow"){
      gzwarn << "[gazebo_optical_flow_plugin] This plugin is intended to work as a Optical Flow camera \n";
  }
  else{
      gzwarn << "[gazebo_optical_flow_plugin] The purpose is not recognized.. plugin doing nothing \n";
      cout << this->camera_name << endl;
  }


  if (_sdf->HasElement("robotNamespace"))
    namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
  else
    gzwarn << "[gazebo_optical_flow_plugin] Please specify a robotNamespace.\n";

  this->node_handle_ = new ros::NodeHandle(namespace_);

  if(this->camera_name == "camera_flow"){

      // Optical Flow publisher
      this->opticalflow_pub =  this->node_handle_->advertise<mavros_msgs::OpticalFlowRad>("px4flow/raw/optical_flow_rad", 10);

      /* Intializes time */
      gettimeofday(&current_time, NULL);
      gettimeofday(&previous_time, NULL);

  }
  if(this->camera_name == "camera_cam"){
      image_transport::ImageTransport it(*(this->node_handle_));
      this->camera_pub = it.advertise("camera/image", 1);
  }

  //this->grounddistance_pub = this->n.advertise<sensor_msgs/Range>("px4flow/ground_distance", 1000);
  //this->temperature_pub = this->n.advertise<sensor_msgs/Temperature>("px4flow/temperature", 1000);   


  this->newFrameConnection = this->camera->ConnectNewImageFrame(
      boost::bind(&CameraPlugin::OnNewFrame, this, _1, this->width, this->height, this->depth, this->format));

  this->parentSensor->SetActive(true);


}

/////////////////////////////////////////////////
void CameraPlugin::OnNewFrame(const unsigned char * _image,
                              unsigned int _width,
                              unsigned int _height,
                              unsigned int _depth,
                              const std::string &_format)
{
  _image = this->camera->GetImageData(0);
  //GetHFOV gives fucking gazebo::math::Angle which you can not cast...
  const double Hfov = 0.6;
  const double focal_length = (_width/2)/tan(Hfov/2);

  double pixel_flow_x_integral = 0.0;
  double pixel_flow_y_integral = 0.0;
  double rate = this->camera->GetRenderRate();
  if (!isfinite(rate))
      rate =  30.0;
  double dt = 1.0 / rate;


  Mat frame = Mat(_height, _width, CV_8UC3);
  Mat frameBGR = Mat(_height, _width, CV_8UC3);
  frame.data = (uchar*)_image; //frame is not the right color now -> convert
  cvtColor(frame, frameBGR, CV_RGB2BGR);

  if(this->camera_name == "camera_flow"){


      cvtColor(frameBGR, frame_gray, CV_BGR2GRAY);

      featuresPrevious = featuresCurrent;

      goodFeaturesToTrack(frame_gray, featuresCurrent, maxfeatures, qualityLevel, minDistance, Mat(), blockSize, useHarrisDetector, k); //calculate the features

      if (!old_gray.empty() && featuresPrevious.size() > 0){
          calcOpticalFlowPyrLK(old_gray, frame_gray, featuresPrevious, featuresNextPos, featuresFound, err);
      }

      /*/// Set the needed parameters to find the refined corners
  Size winSize = Size( 5, 5 );
  Size zeroZone = Size( -1, -1 );
  TermCriteria criteria = TermCriteria( CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 40, 0.001 );

  // Calculate the refined corner locations
  cornerSubPix(frame_gray, featuresNextPos, winSize, zeroZone, criteria);
  cornerSubPix(old_gray, featuresPrevious, winSize, zeroZone, criteria);*/

      // Calculate the average of the Pixel Motion
      int meancount = 0;
      for (int i = 0; i < featuresNextPos.size(); i++) {

          if (featuresFound[i] == true) {
              pixel_flow_x_integral += featuresNextPos[i].x - featuresPrevious[i].x;
              pixel_flow_y_integral += featuresNextPos[i].y - featuresPrevious[i].y;
              meancount++;
          }
      }

      previous_time = current_time;

      gettimeofday(&current_time, NULL);

      double seconds = (double) (current_time.tv_usec - previous_time.tv_usec) / 1000000 +
              (double) (current_time.tv_sec - previous_time.tv_sec);

      //  std::cout << "seconds = " << seconds << endl;

      pixel_flow_x_integral /= meancount;
      pixel_flow_y_integral /= meancount;

      float flow_compx = pixel_flow_x_integral / focal_length / seconds;
      float flow_compy = pixel_flow_y_integral / focal_length / seconds;

      //  std::cout << "flow_comp x = " << flow_compx << "   flow_comp y = " << flow_compy << endl;

      //  float range  = 1;

      //  std::cout << "velocity x = " << new_velocity_x << "   velocity y = " << new_velocity_y << endl;

      //  double flow_x_ang = atan2(pixel_flow_x_integral/meancount, focal_length);
      //  double flow_y_ang = atan2(pixel_flow_y_integral/meancount, focal_length);

      old_gray = frame_gray.clone();

      //  std::cout << "pixel flow x = " << pixel_flow_x_integral << "   pixel flow y = " << pixel_flow_y_integral << endl;

      mavros_msgs::OpticalFlowRad OpticalFlowRad_msg;


      //  OpticalFlowRad_msg.set_time_usec(100000000000000);//big number to prevent timeout in inav
      //  OpticalFlowRad_msg.set_sensor_id(2.0);
      //  OpticalFlowRad_msg.set_integration_time_us(dt * 1000000);
      //  OpticalFlowRad_msg.set_integrated_x(flow_x_ang);
      //  OpticalFlowRad_msg.set_integrated_y(flow_y_ang);
      //  OpticalFlowRad_msg.set_integrated_xgyro(0.0); //get real values in gazebo_mavlink_interface.cpp
      //  OpticalFlowRad_msg.set_integrated_ygyro(0.0); //get real values in gazebo_mavlink_interface.cpp
      //  OpticalFlowRad_msg.set_integrated_zgyro(0.0); //get real values in gazebo_mavlink_interface.cpp
      //  OpticalFlowRad_msg.set_temperature(20.0);
      //  OpticalFlowRad_msg.set_quality(meancount * 255 / maxfeatures); //features?
      //  OpticalFlowRad_msg.set_time_delta_distance_us(0.0);
      //  OpticalFlowRad_msg.set_distance(0.0); //get real values in gazebo_mavlink_interface.cpp

      //opticalFlow_pub_->Publish(opticalFlow_message);

      OpticalFlowRad_msg.integration_time_us = dt * 1000000;
      OpticalFlowRad_msg.integrated_x = flow_compx;
      OpticalFlowRad_msg.integrated_y = flow_compy;
      OpticalFlowRad_msg.integrated_xgyro = 0.0; //get real values in gazebo_mavlink_interface.cpp
      OpticalFlowRad_msg.integrated_ygyro = 0.0; //get real values in gazebo_mavlink_interface.cpp
      OpticalFlowRad_msg.integrated_zgyro = 0.0; //get real values in gazebo_mavlink_interface.cpp
      OpticalFlowRad_msg.temperature = 20.0;
      OpticalFlowRad_msg.quality = meancount * 255 / maxfeatures; //features?
      OpticalFlowRad_msg.time_delta_distance_us = 0.0;
      OpticalFlowRad_msg.distance = 0.0; //get real values in gazebo_mavlink_interface.cpp

      //sensor_msgs::Range Range_msg;
      //sensor_msgs::Temperature Temperature_msg;

      this->opticalflow_pub.publish(OpticalFlowRad_msg	);
      //this->grounddistance_pub.publish(msg);
      //this->temperature_pub.publish(msg);

      //std::cout << "flow_x_and" << flow_x_ang << " | " << "flow_y_ang" <<  flow_y_ang;
  }
  if(this->camera_name == "camera_cam"){
      sensor_msgs::ImagePtr camera_msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frameBGR).toImageMsg();
      this->camera_pub.publish(camera_msg);
  }

}
