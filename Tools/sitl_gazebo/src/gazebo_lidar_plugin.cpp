/*
 * Copyright (C) 2012-2014 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/*
 * Desc: Contact plugin
 * Author: Nate Koenig mod by John Hsu
 */

/*
* Extended Plugin for using both Optical Flow and LIDAR information (px4Flow)
* Author: Alejandro Rodríguez Ramos
* Date: 27th January, 2016
* Email: alejandro.dosr@gmail.com
*
* Description: This extention lets the lidar plugin use the Optical Flow information
* in order to calculate the Gazebo Model velocity. Optical Flow information is retrieved through
* ROS messages communication. Finally, px4Flow calculations are published via ROS Publisher.
*
* File: gazebo_lidar_plugin.cpp
*
*/

/* Includes */

#include "gazebo/physics/physics.hh"
#include "gazebo_lidar_plugin.h"

#include <gazebo/common/common.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include "gazebo/transport/transport.hh"
#include "gazebo/msgs/msgs.hh"

#include <chrono>
#include <cmath>
#include <iostream>
#include <stdio.h>

#define AMPLIFIER 1


using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(RayPlugin)

gazebo::RayPlugin rayPlugin;

void getOpticalFlow(const mavros_msgs::OpticalFlowRad::ConstPtr& msg);

/////////////////////////////////////////////////
/// \brief RayPlugin::RayPlugin
///
RayPlugin::RayPlugin()
{
}

/////////////////////////////////////////////////
/// \brief RayPlugin::~RayPlugin
///
RayPlugin::~RayPlugin()
{
    this->parentSensor->GetLaserShape()->DisconnectNewLaserScans(
                this->newLaserScansConnection);
    this->newLaserScansConnection.reset();

    this->parentSensor.reset();
    this->world.reset();
}

/////////////////////////////////////////////////
/// \brief RayPlugin::Load
/// \param _parent
/// \param _sdf
///
void RayPlugin::Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
{
    // Exit if no ROS
    if (!ros::isInitialized())
    {
        gzerr << "Not loading Lidar plugin since ROS hasn't been "
              << "properly initialized.  Try starting gazebo with ros plugin:\n"
              << "  gazebo -s libgazebo_ros_api_plugin.so\n";
        return;
    }

    // Get then name of the parent sensor
    this->parentSensor =
            boost::dynamic_pointer_cast<sensors::RaySensor>(_parent);

    if (!this->parentSensor)
        gzthrow("RayPlugin requires a Ray Sensor as its parent");

    this->world = physics::get_world(this->parentSensor->GetWorldName());

    this->newLaserScansConnection =
            this->parentSensor->GetLaserShape()->ConnectNewLaserScans(
                boost::bind(&RayPlugin::OnNewLaserScans, this));

    if (_sdf->HasElement("robotNamespace"))
        namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
    else
        gzwarn << "[gazebo_lidar_plugin] Please specify a robotNamespace.\n";

    this->node_handle_ = new ros::NodeHandle(namespace_);

    //  node_handle_ = transport::NodePtr(new transport::Node());
    //  node_handle_->Init(namespace_);

    this->lidar_pub =  this->node_handle_->advertise<sensor_msgs::Range>("px4flow/raw/lidar", 10);
    this->pose_pub =  this->node_handle_->advertise<geometry_msgs::PoseStamped>("px4flow/raw/px4Flow_pose_z", 10);
    this->lidar_sub = this->node_handle_->subscribe("px4flow/raw/optical_flow_rad", 10, getOpticalFlow);
    rayPlugin.px4Flow_pub = this->node_handle_->advertise<geometry_msgs::TwistStamped>("px4flow/raw/px4Flow_speed", 10);

}

/////////////////////////////////////////////////
/// \brief RayPlugin::OnNewLaserScans
///
void RayPlugin::OnNewLaserScans()
{
    sensor_msgs::Range range_msg;
    geometry_msgs::PoseStamped z_msg;

    //Retrieve the ranges and fill the range message
    range_msg.min_range = parentSensor->GetRangeMin();
    range_msg.max_range =  parentSensor->GetRangeMax();
    range_msg.range = parentSensor->GetRange(0);

    z_msg.pose.position.z = range_msg.range;

    //  lidar_message.set_time_msec(0);
    //  lidar_message.set_min_distance(parentSensor->GetRangeMin());
    //  lidar_message.set_max_distance(parentSensor->GetRangeMax());
    //  lidar_message.set_current_distance(parentSensor->GetRange(0));

    //  lidar_pub_->Publish(lidar_message);

    // Store the value in the class member variable
    rayPlugin.range = range_msg.range;


    //  std::cout << "laser_scan: " <<  range << std::endl;

    // Publish the value
    this->lidar_pub.publish(range_msg);
    this->pose_pub.publish(z_msg);
}

/////////////////////////////////////////////////////////////////////////
/// getOpticalFlow: calculates the px4Flow information and publishes it
///
void getOpticalFlow(const mavros_msgs::OpticalFlowRad::ConstPtr& msg){

//    std::cout << "range: " << rayPlugin.range << std::endl;
    // Get the range from the class member variable
    float range = rayPlugin.range;

    // Read the partial calculations
    float flowcompx = msg->integrated_x;
    float flowcompy = msg->integrated_y;

    // Compute the model velocity from its distance to the ground
    float new_velocity_x = - flowcompx *  range * AMPLIFIER;
    float new_velocity_y = - flowcompy *  range * AMPLIFIER;

//    std::cout << "new_velocity_x: " << new_velocity_x << std::endl;
//    std::cout << "new_velocity_y: " << new_velocity_y << std::endl;

//    sensor_msgs::Range px4Flow_msg;
    geometry_msgs::TwistStamped px4Flow_msg;
    px4Flow_msg.twist.linear.x = new_velocity_x;
    px4Flow_msg.twist.linear.y = new_velocity_y;
    px4Flow_msg.header.stamp = ros::Time::now();

    rayPlugin.px4Flow_pub.publish(px4Flow_msg);

}



