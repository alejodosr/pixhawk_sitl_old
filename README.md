

PixHawk SITL with Gazebo Simulation
===================================
A ready-to-use SITL (Software In The Loop) simulation based on Gazebo 6 and using PixHawk as the autopilot. Some new Gazebo plugins and different worlds are already added. Everything is focused on targeting the IMAV 2016 competition.
Everyone is encouraged to improve the plugins here included as well as to generate new worlds or SITL optimizations. It has been tested in ROS Jade.

## Related work ##

- PX4 Flight Core and PX4 Middleware: https://github.com/PX4/Firmware
- Gazebo for PX4 SITL: https://github.com/PX4/sitl_gazebo
- AeroStack: https://bitbucket.org/joselusl/quadrotor_swarm_sub

Instructions
============

## Install Gazebo Simulator

Follow instructions on the [official site](http://gazebosim.org/tutorials?cat=install) to install [IMPORTANT] **Gazebo 6**. Linux users Gazebo 6. Failing to install the right version can render the simulation inoperational. Finally, be sure your have installed also [Gazebo 6 with ROS](http://gazebosim.org/tutorials?tut=ros_wrapper_versions#Gazebo6.xseries). Install [ROS jade](http://wiki.ros.org/jade/Installation/Ubuntu) if you do not have it previously in your computer.

## Protobuf

Install the protobuf library, which is used as interface to Gazebo.

```bash
sudo apt-get install libprotobuf-dev libprotoc-dev protobuf-compiler libeigen3-dev libgazebo6-dev
```
## Genromfs

Install the genromfs.

```bash
cd ~/src
git clone https://github.com/chexum/genromfs
cd genromfs
make
sudo make install
```
## MAVROS messages

Install the genromfs.

```bash
sudo apt-get install ros-jade-mavros-msgs
```

## Build and Launch the Simulation

Clone the repository to your computer. IMPORTANT: If you do not clone to ~/src/pixhawk_sitl, all remaining paths in these instructions will need to be adjusted.

```bash
mkdir -p ~/src
cd src
git clone https://alejodosr@bitbucket.org/alejodosr/pixhawk_sitl.git
```
Next add the location of this build directory to your gazebo plugin path, e.g. add the following line to your .bashrc (Linux).

```bash
# Set the plugin path so Gazebo finds our model and sim
export GAZEBO_PLUGIN_PATH=${GAZEBO_PLUGIN_PATH}:$HOME/src/pixhawk_sitl/Tools/sitl_gazebo/Build
# Set the model path so Gazebo finds the airframes
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:$HOME/src/pixhawk_sitl/Tools/sitl_gazebo/models
# Disable online model lookup since this is quite experimental and unstable
export GAZEBO_MODEL_DATABASE_URI=""
```
Source and Launch roscore.
```bash
source /opt/ros/jade/setup.bash
cd ~/src/pixhawk_sitl
roscore
```
Open a new tab or terminal. Build and launch the simulation.
```bash
source /opt/ros/jade/setup.bash
cd ~/src/pixhawk_sitl
make posix_sitl_default gazebo
```
NOTE: CMake >= 2.8 is required. If last step fails to launch because of this reason, follow the 3rd party option to install CMake 3.2.
IMPORTANT: In order to launch this SITL simulation  with AeroStack, write down the IP which is seen after running the simulation with the command above.
[See this image](https://drive.google.com/open?id=0BxTO9XaQzqwIb29GRWNpeDZGWDg)

## Installing and Linking AeroStack to SITL
Download the AeroStack installation script file (you need to request permissions for this repository before downloading it).
```bash
git clone https://bitbucket.org/joselusl/aerostack_installer ~/temp
```
Run it in a non-ARM Architecture Computer with Linux Ubuntu 14.04 and ROS Jade.
```bash
~/temp/installation_script.sh
```
Link the SITL simulation and the AeroStack.
```bash
cd $DRONE_STACK/launchers/pixhawk_simulation_launchers/launch_files/
gedit px4_SITL.launch
```
Now change the IP *default="udp://:14550@**192.168.1.121**:14556"* by the one you wrote down before and save the file.

## Controlling the model with AeroStack

Open a terminal.
```bash
roscore
```
Open a new tab in a terminal.
```bash
cd ~/src/pixhawk_sitl/
make posix_sitl_default gazebo
```
Open another new tab in a terminal.
```bash
cd $DRONE_STACK/launchers/pixhawk_simulation_launchers
./pixhawk_simulation.sh
```
Now, AeroStack can be used normally (Arm the motors, set mode to OFFBOARD and takeoff normally).To stop the AeroStack (in the same tab where was launched).
```bash
./stop.sh
```

Additional Notes
============

## World edition in Gazebo

To edit the world in Gazebo without launching the SITL simulation, do the following.

```bash
cd ~/src/pixhawk_sitl/Tools/sitl_gazebo/worlds
gazebo <any_world>.world
```

## Launch SITL with another pre-designed world

Edit sitl_run.sh.

```bash
gedit ~/src/pixhawk_sitl/Tools/sitl_run.sh
```
And comment/uncomment the *rosrun gazebo_ros gazebo <pre-designed-world>.world* you would like to test.
